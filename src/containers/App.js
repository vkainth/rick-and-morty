import React, {Component} from 'react';
import {CharacterList, Header} from '../components';
import {
	Container,
	Pagination,
	PaginationItem,
	PaginationLink,
	Jumbotron
} from 'reactstrap';
import './App.css';

class App extends Component {
	constructor() {
		super();
		this.state = {
			baseUrl: 'https://rickandmortyapi.com/api/character/?page=',
			url: 'https://rickandmortyapi.com/api/character/?page=1',
			nextUrl: 'https://rickandmortyapi.com/api/character/?page=2',
			prevUrl: undefined,
			numPages: 25,
			results: []
		};
	}

	componentDidMount() {
		document.title = 'Rick and Morty';
		this.fetchCharacters();
	}

	fetchCharacters() {
		fetch(this.state.url)
			.then(resp => resp.json())
			.then(value => {
				this.setState({results: value.results});
				this.setState({url: value.results.url});
				this.setState({nextUrl: value.info.next});
				this.setState({prevUrl: value.info.prev || undefined});
			});
	}

	renderCharacter() {
		return <CharacterList results={this.state.results}/>;
	}

	renderLoading() {
		return (
		<div class="lds-ring">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			</div>
			);
	}

	onPageClick(url, event) {
		this.setState({url: url});
		this.setState({results: []}, this.fetchCharacters);
	}

	renderPagination() {
		let pages = [];
		pages.push(
			<PaginationItem key={0}>
				<PaginationLink
					onClick={this.onPageClick.bind(this, this.state.prevUrl)}
					previous
					href="#"
				/>
			</PaginationItem>
		);
		for (let i = 1; i <= this.state.numPages; i++) {
			const currentUrl = this.state.baseUrl + i.toString();
			pages.push(
				<PaginationItem key={i.toString()}>
					<PaginationLink
						onClick={this.onPageClick.bind(this, currentUrl)}
						href="#"
					>
						{i}
					</PaginationLink>
				</PaginationItem>
			);
		}
		pages.push(
			<PaginationItem key={26}>
				<PaginationLink
					onClick={this.onPageClick.bind(this, this.state.nextUrl)}
					next
					href="#"
				/>
			</PaginationItem>
		);
		return (
			<Pagination className="table-responsive" aria-label="Page navigation">
				{pages}
			</Pagination>
		);
	}

	render() {
		let renderComponent;
		if (this.state.results.length === 0) {
			renderComponent = (
				<div className="d-flex justify-content-center align-items-center">
					{this.renderLoading()}
				</div>
			);
		} else {
			renderComponent = <Container>{this.renderCharacter()}</Container>;
		}
		return (
			<div>
				<Header/>
				<Jumbotron className="text-center bg-white">
					<Container>
						<h1 className="jumbotron-heading">
							Rick And Morty Characters List
						</h1>
						<p className="lead text-muted">
							This website uses the{' '}
							<a href="http://rickandmortyapi.com/">Rick and Morty API</a> to
							present you with all the extant (and not so much) characters in
							the Rick and Morty universe.{' '}
						</p>
					</Container>
				</Jumbotron>
				<div className="album py-5 bg-light">{renderComponent}</div>
				<footer className="text-muted">
					<Container>{this.renderPagination()}</Container>
				</footer>
			</div>
		);
	}
}

export {App};
