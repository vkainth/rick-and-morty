import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {CharacterItem} from '../components';
import {
	Card,
	CardImg,
	CardTitle,
	Col,
	CardSubtitle,
	Button,
	Collapse
} from 'reactstrap';

class Character extends Component {
	constructor(props) {
		super(props);
		this.toggle = this.toggle.bind(this);
		this.state = {
			collapse: false
		};
	}

	toggle() {
		this.setState({collapse: !this.state.collapse});
	}

	render() {
		return (
			<Col md="4">
				<Card className="mb-4 box-shadow text-center">
					<CardImg
						top
						height="200px"
						width="100%"
						src={this.props.image}
						alt="Avatar"
					/>
					<CardTitle>{this.props.name}</CardTitle>
					<div className="d-flex justify-content-around">
						<CardSubtitle className="p-2">{this.props.status}</CardSubtitle>
						<Button
							outline
							size="sm"
							color="secondary"
							onClick={this.toggle}
							type="button"
						>
							More
						</Button>
					</div>

					<Collapse isOpen={this.state.collapse}>
						<CharacterItem
							className="p-2"
							name="Species"
							value={this.props.species}
						/>
						<CharacterItem
							className="p-2"
							name="Type"
							value={this.props.type}
						/>
						<CharacterItem
							className="p-2"
							name="Gender"
							value={this.props.gender}
						/>
						<CharacterItem
							className="p-2"
							name="Origin"
							value={this.props.origin}
						/>
						<CharacterItem
							className="p-2"
							name="Last Location"
							value={this.props.lastLocation}
						/>
						<CharacterItem
							className="p-2"
							name="Episodes"
							value={this.props.numEpisodes}
						/>
					</Collapse>
				</Card>
			</Col>
		);
	}
}

Character.propTypes = {
	name: PropTypes.string,
	status: PropTypes.string,
	species: PropTypes.string,
	type: PropTypes.string,
	gender: PropTypes.string,
	origin: PropTypes.string,
	lastLocation: PropTypes.string,
	image: PropTypes.string,
	numEpisodes: PropTypes.number
};

Character.defaultProps = {
	name: 'Alan',
	status: 'Alive',
	species: 'Humanoid',
	type: 'Human',
	gender: 'Unknown',
	origin: 'Nostradamus',
	lastLocation: 'Unknown',
	image: 'https://www.url-for-some-image.png',
	numEpisodes: 0
};
export {Character};
