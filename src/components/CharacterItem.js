import React from 'react';
import PropTypes from 'prop-types';
import {Col, Row, Container} from 'reactstrap';

const CharacterItem = ({name, value}) => {
	return (
		<Container>
			<Row className="py-2 border-bottom">
				<Col className="text-uppercase font-weight-bold" lg="6">
					{name}
				</Col>
				<Col lg="6">{value}</Col>
			</Row>
		</Container>
	);
};

CharacterItem.propTypes = {
	name: PropTypes.string,
	value: PropTypes.string
};

CharacterItem.defaultProps = {
	name: 'Species',
	value: 'Unknown'
};

export {CharacterItem};
