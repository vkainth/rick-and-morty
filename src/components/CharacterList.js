import React from 'react';
import PropTypes from 'prop-types';
import {Character} from '../components';
import {Row} from 'reactstrap';

const CharacterList = ({results}) => {
	let characters = [];
	for (let i = 0; i < results.length; i++) {
		const {
			name,
			status,
			species,
			type,
			gender,
			origin,
			location,
			image,
			episode
		} = results[i];
		let character = (
			<Character
				key={i}
				name={name || 'unknown'}
				status={status || 'unknown'}
				species={species || 'unknown'}
				type={type || 'unknown'}
				gender={gender || 'unknown'}
				origin={origin.name || 'unknown'}
				lastLocation={location.name || 'unknown'}
				image={image}
				numEpisodes={episode.length}
			/>
		);
		characters.push(character);
	}
	return <Row>{characters}</Row>;
};

CharacterList.propTypes = {
	results: PropTypes.array
};

CharacterList.defaultProps = {
	results: []
};

export {CharacterList};
