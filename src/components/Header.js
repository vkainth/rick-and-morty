import React, {Component} from 'react';
import {
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Container,
	Row,
	Col
} from 'reactstrap';

class Header extends Component {
	constructor() {
		super();
		this.toggleNavbar = this.toggleNavbar.bind(this);
		this.state = {
			collapsed: true
		};
	}

	toggleNavbar() {
		this.setState({collapsed: !this.state.collapsed});
	}

	render() {
		return (
			<div>
				<Navbar color="dark" dark>
					<NavbarBrand href="https://vkainth.gitlab.io" className="mr-auto">
						@vkainth
					</NavbarBrand>
					<NavbarToggler onClick={this.toggleNavbar} className="mr-2"/>
					<Collapse isOpen={!this.state.collapsed} navbar>
						<Container>
							<Row>
								<Col md="7" sm="8" className="py-4">
									<h4 className="text-white">About</h4>
									<p className="text-muted">
										Vibhor Kainth is a software engineer living in Arlington,
										TX. He is always on the lookout for awesome software
										opportunities.
									</p>
								</Col>
								<Col sm="4" className="offset-md-1 py-4">
									<h4 className="text-white">Contact</h4>
									<ul className="list-unstyled">
										<li>
											<a
												className="text-white"
												href="https://gitlab.com/vkainth"
											>
												Gitlab
											</a>
										</li>
										<li>
											<a
												className="text-white"
												href="https://vkainth.gitlab.io"
											>
												Blog
											</a>
										</li>
										<li>
											<a
												className="text-white"
												href="mailto:vibhor.kainth01@gmail.com&commat;example.org?subject=Hi"
											>
												Email
											</a>
										</li>
									</ul>
								</Col>
							</Row>
						</Container>
					</Collapse>
				</Navbar>
			</div>
		);
	}
}

export {Header};
