# Rick and Morty Characters

[https://vkainth.gitlab.io/rick-and-morty](https://vkainth.gitlab.io/rick-and-morty)

## Introduction

The Rick and Morty characters application is a straightforward one: it fetches
data from an API and renders all the characters using React.

## Installation

- Clone the repo on your workstation `git clone https://github.com/vibhor-kainth/rick-and-morty.git`

- Run `cd rick-and-morty` from the terminal

- Run `yarn install` or `npm install` from the terminal

- Run `yarn start` or `npm run start` to start a local development server

## Production Build

- Ensure that you are in the `rick-and-morty` parent directory

- Run `yarn build` or `npm run build` to create a production build

- You can use [serve](https://github.com/zeit/serve) to serve the build

  - Run `yarn global add serve` or `npm install -g serve` to install serve

  - Run `serve -s build/` to start a static server

## Reason

To improve my knowledge of react and APIs and help me become a better developer.

## Attribution

This application was created by [create-react-app](https://github.com/facebook/create-react-app)
and then styled using [reactstrap](http://reactstrap.github.io/) and [Bootstrap](https://getbootstrap.com/)
and the gradient is by [ui-gradients](https://uigradients.com/). The data is
fetched from the [Rick and Morty API](http://rickandmortyapi.com/).
